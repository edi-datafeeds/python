

import csv
import pymysql
import datetime
import fnmatch
import os

fpath = 'O:/Prodman/Dev/WFI/Feeds/WMDaten/Out/'

for file in os.listdir(fpath):
    if fnmatch.fnmatch(file, '*.csv'):
        loadfile = (os.path.join(fpath,file))

#print(loadfile)

# Load text file into list with CSV module
with open(loadfile, 'rt') as f:
    reader = csv.reader(f)
    lineData = list()
    next(reader)
    next(reader)


    for line in reader:
        if line != []:
            lineData.append(line)

# Connect with database
cnx = pymysql.connect(user='sa', password='K376:lcnb',
                      host='192.168.2.60',
                      database='portfolio')
cursor = cnx.cursor()

# Writing Query to insert data
query = ("INSERT IGNORE INTO WM_Port "
         "(ISIN, SPOTDATE) "
         "VALUES (%s, NOW())")

# Change every item in the sub list into the correct data type and store it in a directory
for i in range(len(lineData)):
    wm = (lineData[i][0])
    cursor.execute(query, wm)  # Execute the Query

# Commit the query
cnx.commit()
cnx.close()
